

import java.util.Observable;
import java.util.Observer;

/**
 * NOTE: Observer interface is deprecated since Java 9.
 *       https://docs.oracle.com/javase/8/docs/api/java/beans/PropertyChangeSupport.html
 */

/**
 * This is our obervable - it has state of interest.
 * When our news data changes, all obersvers receive notification
 * It keeps track of any observers.
 */


public class NewsAgency extends Observable {
  private String news;

  /**
   * We call Observables' methods setChanged() & notifyObservers()
   * directly in our setState. We could separate these into 
   * another method if need be
   */

  public void setState(String s) {
    this.news = s;
    setChanged(); 
    notifyObservers();
  }
  
  
  /*
  public void sendData(){
      setChanged();
      notifyObservers();
  }
  */

  public String getNews() {
    return this.news;
  }
}

/**
 * NOTE : setChanged() boolean method
 *       could be used to optimise notifications.
 *       Eg. only notify when data/state reaches a threshold
 *
 *
 */
 