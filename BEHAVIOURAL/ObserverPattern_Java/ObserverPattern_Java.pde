/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : ObserverPattern_Java
 *
 * Summary : Observer Pattern keeps everyone in the know.
 *           This version implements Java's in-built classes.
 *
 */


/////////////////////////// GLOBALS ////////////////////////////
NewsAgency ag = new NewsAgency();
// we keep a reference of our observable with our NewsChannel
// we also register our channel with the observable at instantiation
NewsChannel ch = new NewsChannel(ag);

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(640, 420);
  background(0, 0, 33);
  smooth();
  ag.setState("News...");
  ch.displayNews();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0, 0, 33);
  info();
}

/////////////////////////// FUNCTIONS ////////////////////////////

void keyPressed(){
    if(key == 'n'){
      changeNews();
      ch.displayNews();
    }
    if(key == 'r'){
      ch.removeObs();
    }
    if(key == 's'){
     ch.registerObs();
  }
}

// on each change of state we notify subscribed observers
void changeNews(){
    String[] someTitles = {"It's sunny","It's gonna snow","No news", "Peace at last", "More of the same"};
    String todaysNews = someTitles[(int)random(someTitles.length)];
    ag.setState(todaysNews);
}

// just screen info
void info(){
  fill(255);
  textSize(63);
  text(ch.getNews(), 50, 100);
  textSize(18);
  text(ch.getInfo(), 50, 140);
  textSize(13);
  text("Press 'n' to get news", 50, 260);
  text("Press 'r' to unsubscribe", 50, 275);
  text("Press 's' to subscribe", 50, 290);
}