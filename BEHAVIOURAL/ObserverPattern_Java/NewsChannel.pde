/**
 * This is our observer - it has interest of state.
 * When our news channel has subscribed to an observable
 * it receives state from that observable
 */

public class NewsChannel implements Observer {
  private String news;
  private String info;
  private Observable channelObs;

  NewsChannel(Observable o) {
    this.channelObs = o;
    this.channelObs.addObserver(this);
    info = "You are automatically registered.";
  }

  @Override
    public void update(Observable o, Object arg) {
    if (o instanceof NewsAgency) {
      NewsAgency ag = (NewsAgency) o;
      this.news = ag.getNews();
    }
  }

  public void displayNews() {
    println(this.news);
  }

  public void removeObs() {
    this.channelObs.deleteObserver(this);
    info = "You just removed the observer from the observable";
  }

  public void registerObs() {
    this.channelObs.addObserver(this);
    info = "You just registered the observer to the observable";
  }

  public String getInfo() {
    return info;
  }

  public String getNews() {
    return this.news;
  }
}