import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ObserverPattern extends PApplet {

/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : ObserverPattern
 *
 * Summary : Observer Pattern keeps everyone in the know.
 *
 *
 */


/////////////////////////// GLOBALS ////////////////////////////
NewsAgency ag = new NewsAgency();
// we keep a reference of our observable with our NewsChannel
// we also register our channel with the observable at instantiation
NewsChannel ch = new NewsChannel(ag);

/////////////////////////// SETUP ////////////////////////////
public void setup() {
  
  background(0, 0, 33);
  
  ag.setState("News...");
  ag.notifyObservers();
  ch.displayNews();
}

/////////////////////////// DRAW ////////////////////////////
public void draw() {
  background(0, 0, 33);
  info();
}

/////////////////////////// FUNCTIONS ////////////////////////////

public void keyPressed(){
    if(key == 'n'){
      changeNews();
      ch.displayNews();
    }
    if(key == 'r'){
      ch.removeObs();
    }
    if(key == 's'){
     ch.registerObs();
  }
}

// on each change of state we notify subscribed observers
public void changeNews(){
    String[] someTitles = {"It's sunny","It's gonna snow","No news", "Peace at last", "More of the same"};
    String todaysNews = someTitles[(int)random(someTitles.length)];
    ag.setState(todaysNews);
    ag.notifyObservers();
}

// just screen info
public void info(){
  fill(255);
  textSize(63);
  text(ch.getNews(), 50, 100);
  textSize(18);
  text(ch.getInfo(), 50, 140);
  textSize(13);
  text("Press 'n' to get news", 50, 260);
  text("Press 'r' to unsubscribe", 50, 275);
  text("Press 's' to subscribe", 50, 290);
}
/**
 * This is our obervable - it has state of interest.
 * When our news data changes, all obersvers receive notification
 * It keeps track of any observers (Channel)
 */


public class NewsAgency implements Observable{
   private String news;
   private ArrayList<Observer> theObservers = new ArrayList();

   public void addObserver(Observer o) {
       this.theObservers.add(o);
   }

   public void removeObserver(Observer o) {
       this.theObservers.remove(o);
   }

   public void notifyObservers() {
       this.news = news;
       for (Observer o : theObservers) {
           o.update(this.news);
       }
   }

   public void setState(String s){
        this.news = s;
   }
}
/**
 * This is our observer - it has interest of state.
 * When our news channel has subscribed to an observable
 * it receives state from that observable
 */

public class NewsChannel implements Observer {
  private String news;
  private String info;
  private Observable channelObs;

  NewsChannel(Observable o) {
    this.channelObs = o;
    this.channelObs.addObserver(this);
    info = "You are automatically registered.";
  }

  @Override
    public void update(String news) {
    this.news = news;
  }

  public void displayNews() {
    println(this.news);

  }

  public void removeObs() {
    this.channelObs.removeObserver(this);
    info = "You just removed the observer from the observable";
  }

  public void registerObs() {
    this.channelObs.addObserver(this);
    info = "You just registered the observer to the observable";
  }

  public String getInfo(){
    return info;
  }

  public String getNews(){
    return this.news;
  }
}
/**
 *
 */

 public interface Observable {
     public void addObserver(Observer channel);
     public void removeObserver(Observer channel);
     public void notifyObservers();
 }

 public interface Observer {
     public void update(String s);
 }
  public void settings() {  size(640, 420);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#000000", "--hide-stop", "ObserverPattern" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
