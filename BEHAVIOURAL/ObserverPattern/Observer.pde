/**
 *
 */

 public interface Observable {
     public void addObserver(Observer channel);
     public void removeObserver(Observer channel);
     public void notifyObservers();
 }

 public interface Observer {
     public void update(String s);
 }
