/**
 * This is our obervable - it has state of interest.
 * When our news data changes, all obersvers receive notification
 * It keeps track of any observers (Channel)
 */


public class NewsAgency implements Observable{
   private String news;
   private ArrayList<Observer> theObservers = new ArrayList();

   public void addObserver(Observer o) {
       this.theObservers.add(o);
   }

   public void removeObserver(Observer o) {
       this.theObservers.remove(o);
   }

   public void notifyObservers() {
       this.news = news;
       for (Observer o : theObservers) {
           o.update(this.news);
       }
   }

   public void setState(String s){
        this.news = s;
   }
}
