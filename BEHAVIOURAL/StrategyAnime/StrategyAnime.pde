/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : StrategyAnime
 *
 * Summary : Strategy Pattern
 *           Implements simple animation algorithms
 *
 */


/////////////////////////// GLOBALS ////////////////////////////
Agent ag1;
Agent ag2;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(640, 420);
  background(0);
  smooth();
  noStroke();
  
  ag1 = new Agent();
  ag1.setBehaviourStrategy(new XOscillate());
  ag1.BSettings(0.05, 200);
  ag2 = new Agent();
  ag2.setBehaviourStrategy(new YOscillate());
  ag2.BSettings(0.075, 150);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0,0,33);
  noStroke();
  ag1.runBStrategy(new PVector(width/2, height/2));
  ag1.drawAnime();
  
  ag2.runBStrategy(new PVector(width/2, height/2));
  ag2.drawAnime();
  
  stroke(255);
  line(width/2, 0, width/2, height);

}

/////////////////////////// FUNCTIONS ////////////////////////////