/**
 * Animation Parent Abstract Class
 */


public abstract class Animation {
  public BehaviourStrategy bs;
  PVector pos;

  Animation() {
    this.pos = new PVector();
  }
  
  
  public void setBehaviourStrategy(BehaviourStrategy bs){
     this.bs = bs;
  }

  public void BSettings(float v1, float v2) {
        bs.BSettings(v1, v2);
  }

  public void runBStrategy(PVector pos) {
    this.pos = pos;
    bs.runBStrategy(this.pos);
  }
 
  public void drawAnime(){
      fill(0,0,255);
      ellipse(this.pos.x, this.pos.y, 15, 15);
  }
}