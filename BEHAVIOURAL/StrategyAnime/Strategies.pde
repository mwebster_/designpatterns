/**
 * Behaviour Interface & Implementations
 */


interface BehaviourStrategy {
  public void BSettings(float v1, float v2);
  public void runBStrategy(PVector pos);
  public String type();
}


public class XOscillate implements  BehaviourStrategy {
  private float freq, amp;
  public String type;
  public void BSettings(float v1, float v2) {
    this.freq = v1;
    this.amp = v2;
    println("You are using the XOscillate algorithm");
  };
  public void runBStrategy(PVector pos) {
    pos.x += cos(frameCount*freq) * amp;
  }
  public String type() {
    return type;
  }
}


public class YOscillate implements  BehaviourStrategy {
  private float freq, amp;
  public String type;
  public void BSettings(float v1, float v2) {
    this.freq = v1;
    this.amp = v2;
    println("You are using the YOscillate algorithm");
  };
  public void runBStrategy(PVector pos) {
    pos.y += sin(frameCount*freq) * amp;
  }
  public String type() {
    return type;
  }
}