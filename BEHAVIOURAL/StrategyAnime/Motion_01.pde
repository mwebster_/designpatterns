/**
 * An extended class
 */


class Agent extends Animation {

  /**
   * @overide 
   */
  public void drawAnime() {
    rectMode(CENTER);
    fill(200, 255, 0);
    rect(this.pos.x, this.pos.y, 35, 35);
  }
}