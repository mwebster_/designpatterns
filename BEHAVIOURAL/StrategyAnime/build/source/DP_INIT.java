import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.io.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class DP_INIT extends PApplet {

/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : ????????????
 *
 * Summary :
 *
 *
 */


/////////////////////////// GLOBALS ////////////////////////////


String filePath = "/Users/tisane/Desktop/AAA_DesignPatters/DP_INIT/data/tx.rtf";
/////////////////////////// SETUP ////////////////////////////

public void setup() {
  
  background(0);
  
  noStroke();
  int c;
  try{
    InputStream in =
      new LowerCase(
        new BufferedInputStream(
          new FileInputStream(filePath)));
          while((c = in.read()) >=0){
            println(PApplet.parseChar(c));
          }
          in.close();
      } catch(IOException e){
    e.printStackTrace();
  }
}

/////////////////////////// DRAW ////////////////////////////
public void draw() {
  //background(255);
}

/////////////////////////// FUNCTIONS ////////////////////////////

public class LowerCase extends FilterInputStream {
  public LowerCase(InputStream in){
    super(in);
  }

  public int read() throws IOException{
    int c = super.read();
    return (c == -1 ? c : Character.toLowerCase((char)c));
  }

  public int read(byte[] b, int offSet, int len) throws IOException{
    int result = super.read(b, offSet, len);
    for(int i=offSet; i < offSet+result; i++){
      b[i] = (byte)Character.toLowerCase((char)b[i]);
    }
    return result;
  }
}
  public void settings() {  size(640, 420);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "DP_INIT" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
