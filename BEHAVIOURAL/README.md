# Behavioural Patterns

Behavioural patterns are used for setting up communication between objects. We often use them at runtime to change states and behaviours.

## Contents

* *Strategy Pattern*

This pattern is used to define a variety of behaviours for objects that share a common abstract class.
It can be seen as a method for assigning different algorithms to objects. We say that the abstract class
is composed of a strategy. The strategy is implemented as an interface. This keeps any changed or additions to behavioural methods separate from the abstract concrete class. We use the strategy pattern when we need to change the behaviour of the object at runtime. For example, exporting files in different formats, sorting content or applying various motion algorithms to a set of coordinates. We could write a strategy pattern for any of these situations. In short, the strategy pattern enables us to encapsulate a family of algorithms that are interchangeable.

![strategyPattern.png](https://bitbucket.org/repo/ype5Ab6/images/3029568081-strategyPattern.png)

* *Observer Pattern*

This pattern keeps all objects in the know. It specifies communication between objects; *observers* & *observables*.
An *observer* is any object that may subscribe to getting state from another object. An *observable* is any object
that has state of interest and to whom other objects may subscribe to receive that state.

The Observer pattern defines a one-to-many dependency between objects. When a state changes, all dependencies are updated accordingly. Observers can ‘subscribe’ and ‘unsubscribe’ to an observable. An observable is a single class/interface that holds state & therefore data. It need not know anything of the observer classes, only that it must send update information once a state changes and only to those observers that are subscribed. This relationship is known as a loosely coupled design and is a good design to strive for when objects interact with each other.

![observerPattern.png](https://bitbucket.org/repo/ype5Ab6/images/1027177306-observerPattern.png)

## Install

The programs in this repository must be compiled using the Processing environment.

https://processing.org/

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.25
* Tools used : Processing

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
