/*
 * ::::::::::::::::::::::::::::
 * DESIGN PATTERNS & STRATEGIES
 * ::::::::::::::::::::::::::::
 *
 * Sketch : Builder_01
 *
 * Summary : The Builder pattern is useful for classes that have many
 *           parameters that are optional or need to be implemented as default.
 */

/////////////////////////// GLOBALS ////////////////////////////

/////////////////////////// SETUP ////////////////////////////

void setup() {
  background(0);
  smooth();
  noStroke();
  // Create our object and call the builder
  // This is a default set of params
  Letter theLetterA = new Letter.LetterBuilder().build();
  theLetterA.printInfo();

  // With this object, we define some params
  Letter theLetterB = new Letter.LetterBuilder()
    .setFont("Helvetica")
    .setMsg("Hello")
    .setColour(color(255, 0, 0)).build();

  theLetterB.printInfo();
}

/////////////////////////// DRAW ////////////////////////////
void draw() { }

/////////////////////////// CLASS ////////////////////////////
public static class Letter {
  private final String font;
  private final String msg;
  private final color theColour;
  private final int ID;

  // This is a nested class
  public static class LetterBuilder {
    private  String font;
    private  String msg;
    private  color theColour;
    private  int ID;


    // creates a default object with default params
    public LetterBuilder() {
      this.font = "ADVENT";
      this.msg = "This is a default message";
      this.theColour = 0;
      this.ID = 0;
    }

    //A list of setter methods for changing default values
    public LetterBuilder setFont(String f) {
      this.font = f;
      return this;
    }

    public LetterBuilder setMsg(String m) {
      this.msg = m;
      return this;
    }

    public LetterBuilder setColour(color c) {
      this.theColour = c;
      return this;
    }
    
     public LetterBuilder setID(int v) {
      this.ID = v;
      return this;
    }

    public Letter build() {
      // call the private constructor in the outer class
      return new Letter(this);
    }
  }

  // this is a private class to instantiate 
  private Letter(LetterBuilder builder) {
    this.font = builder.font;
    this.msg = builder.msg;
    this.theColour = builder.theColour;
    this.ID = builder.ID;
  }

  public String getFont() {
    return font;
  }
  
   public String getMsg() {
    return msg;
  }
  
  public color getColour() {
    return theColour;
  }
  
   public int getID() {
    return ID;
  }

  public void printInfo() {
    println("////////////////////");
    println("FONT: " + this.font);
    println("MESSAGE: " + this.msg);
    println("COLOUR: " + this.theColour);
    println("////////////////////");
  }
}