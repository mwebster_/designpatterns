# DesignPatterns
Organising The particulars.

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse) 
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---

## The Implementation of Ideas as Programs

A computer program is essentially a written text defining processes and there are three fundamental parts that make up those processes. At the highest level, we have problem solving, without which our ideas could not take form and a process can not advance. In the realm of programming we call this part the algorithm - a method for solving a problem. In order to write these algorithms we need the help of two other vital parts that constitute the bare bones of a program: Data and control structures. In a nutshell, we could boil down all computer programs to data and control structures: One stores the stuff - a number, an image, a vector, a colour - whilst the other deals with processing this stuff.

The term structure is important here because essentially a process is about things that are related to other things. We are concerned therefore with the relationships in a program between each component. Being able to understand those relationships is vital if ever you want to really get to grips with writing your own programs and translating thoughts into ideas that become fully working programs that implement those thoughts. Structure is what gives thoughts the possibility to evolve and this on all levels of any process. So, that means getting organised too!

This repository is a collection of simple examples for implementing common design patterns. It is written for Processing users. 

## Contents

* [Behavioural]()
* [Creation]()
* [Structure]()
* Tools

## Install

The programs in this repository must be compiled using the Processing environment.

https://processing.org/

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.25
* Tools used : Processing

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/

